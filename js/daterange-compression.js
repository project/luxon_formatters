/**
 * @file
 * Attaches behaviors for compressing date range fields.
 */

((Drupal, DateTime) => {
  /**
   * Compress date range field output.
   *
   * Assumes time elements have already been localized by
   * Drupal.behaviors.luxonDateTime.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Attaches the behavior to the right context.
   */
  Drupal.behaviors.luxonDateRangeCompression = {
    attach(context) {
      const elements = context.querySelectorAll(
        ".luxon-date-range.with-compression"
      );
      elements.forEach((element) => {
        const [startTime, endTime] = element.querySelectorAll("time");
        const startDateTime = DateTime.fromISO(
          startTime.getAttribute("datetime")
        );
        const endDateTime = DateTime.fromISO(endTime.getAttribute("datetime"));
        // Proceed only if both dates fall on the same calendar day.
        if (startDateTime.hasSame(endDateTime, "day")) {
          // Split date strings into date parts for comparison.
          const startTextParts = startTime.textContent.split(" ");
          const endTextParts = endTime.textContent.split(" ");
          // Remove repeated date parts.
          let diffReached = false;
          for (let i = 0; i < startTextParts.length; i++) {
            if (startTextParts[i] === endTextParts[i] && !diffReached) {
              endTextParts[i] = undefined;
            } else if (startTextParts[i] === endTextParts[i] && diffReached) {
              startTextParts[i] = undefined;
            } else {
              diffReached = true;
            }
          }
          // Update time elements with the resulting compressed date strings.
          startTime.textContent = startTextParts
            .filter((n) => n !== undefined)
            .join(" ");
          endTime.textContent = endTextParts
            .filter((n) => n !== undefined)
            .join(" ");
        }
      });
    },
  };
})(Drupal, luxon.DateTime);

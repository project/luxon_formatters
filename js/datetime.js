/**
 * @file
 * Attaches behaviors for the 'Luxon custom' formatter for 'datetime' fields.
 */

((Drupal, DateTime) => {
  /**
   * Localize datetime fields using luxon.DateTime.toFormat().
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *  Attaches the behavior to the right context.
   */
  Drupal.behaviors.luxonDateTime = {
    attach(context) {
      const elements = context.querySelectorAll(
        "time[datetime][data-luxon-preset], time[datetime][data-luxon-format]"
      );
      elements.forEach((element) => {
        const datetime = element.getAttribute("datetime");
        const preset = element.dataset.luxonPreset;
        const format = element.dataset.luxonFormat;
        if (datetime && preset) {
          element.textContent = DateTime.fromISO(datetime).toLocaleString(
            DateTime[preset]
          );
        } else if (datetime && format) {
          element.textContent = DateTime.fromISO(datetime).toFormat(format);
        }
      });
    },
  };
})(Drupal, luxon.DateTime);

<?php

namespace Drupal\luxon_formatters;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides shared methods for daterange Luxon field formatters.
 */
trait DateRangeTrait {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'separator' => '-',
      'compress' => FALSE,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date separator'),
      '#description' => $this->t('The string to separate the start and end dates'),
      '#default_value' => $this->getSetting('separator'),
    ];

    $form['compress'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Compress'),
      '#description' => $this->t('Reduce date part repetition when start and end dates fall on the same day (eg. "October 14, 1983 at 1:30 PM EDT - October 14, 1983 at 2:30 PM EDT" becomes "October 14, 1983 at 1:30 - 2:30 PM EDT"'),
      '#default_value' => $this->getSetting('compress'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    if ($separator = $this->getSetting('separator')) {
      $summary[] = $this->t('Separator: %separator', ['%separator' => $separator]);
    }
    if ($separator = $this->getSetting('compress')) {
      $summary[] = $this->t('Compression on');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $separator = $this->getSetting('separator');
    $compress = $this->getSetting('compress');

    foreach ($items as $delta => $item) {
      if (!empty($item->start_date) && !empty($item->end_date)) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $start_date */
        $start_date = $item->start_date;
        /** @var \Drupal\Core\Datetime\DrupalDateTime $end_date */
        $end_date = $item->end_date;

        if ($start_date->getTimestamp() !== $end_date->getTimestamp()) {
          $elements[$delta] = [
            '#type' => 'html_tag',
            '#tag' => 'div',
            'start_date' => $this->buildDateWithIsoAttribute($start_date),
            'separator' => ['#plain_text' => ' ' . $separator . ' '],
            'end_date' => $this->buildDateWithIsoAttribute($end_date),
          ];
          if ($compress) {
            // Attach compression library and class.
            $elements[$delta]['#attached']['library'][] = 'luxon_formatters/daterange_compression';
            $elements[$delta]['#attributes']['class'][] = 'with-compression';
          }
        }
        else {
          $elements[$delta] = $this->buildDateWithIsoAttribute($start_date);

          if (!empty($item->_attributes)) {
            $elements[$delta]['#attributes'] += $item->_attributes;
            // Unset field item attributes since they have been included in the
            // formatter output and should not be rendered in the field
            // template.
            unset($item->_attributes);
          }
        }
        $elements[$delta]['#attributes']['class'][] = 'luxon-date-range';
      }
    }

    return $elements;
  }

}

<?php

namespace Drupal\luxon_formatters;

use Drupal\Core\Field\FieldItemListInterface;

/**
 * Provides shared methods for Luxon datetime field formatters.
 */
trait DateTimeTrait {

  /**
   * {@inheritdoc}
   */
  public function view(FieldItemListInterface $items, $langcode = NULL) {
    $elements = parent::view($items, $langcode);

    if (!empty($elements)) {
      // Attach this formatter's base library.
      $elements['#attached']['library'][] = 'luxon_formatters/datetime';
    }

    return $elements;
  }

}

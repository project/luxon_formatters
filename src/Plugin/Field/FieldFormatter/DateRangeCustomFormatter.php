<?php

namespace Drupal\luxon_formatters\Plugin\Field\FieldFormatter;

use Drupal\luxon_formatters\DateRangeTrait;

/**
 * Plugin implementation of the 'Luxon custom' formatter for 'daterange' fields.
 *
 * @FieldFormatter(
 *   id = "luxon_formatters_daterange_custom",
 *   label = @Translation("Luxon custom"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangeCustomFormatter extends DateTimeCustomFormatter {

  use DateRangeTrait;

}

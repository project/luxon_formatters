<?php

namespace Drupal\luxon_formatters\Plugin\Field\FieldFormatter;

use Drupal\luxon_formatters\DateRangeTrait;

/**
 * Plugin implementation of the 'Luxon preset' formatter for 'daterange' fields.
 *
 * @FieldFormatter(
 *   id = "luxon_formatters_daterange_preset",
 *   label = @Translation("Luxon preset"),
 *   field_types = {
 *     "daterange"
 *   }
 * )
 */
class DateRangePresetFormatter extends DateTimePresetFormatter {

  use DateRangeTrait;

}

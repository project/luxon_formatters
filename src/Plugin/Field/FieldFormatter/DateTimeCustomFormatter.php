<?php

namespace Drupal\luxon_formatters\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeFormatterBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\luxon_formatters\DateTimeTrait;

/**
 * Plugin implementation of the 'Luxon custom' formatter for 'datetime' fields.
 *
 * @FieldFormatter(
 *   id = "luxon_formatters_datetime_custom",
 *   label = @Translation("Luxon custom"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DateTimeCustomFormatter extends DateTimeFormatterBase {

  use DateTimeTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_format' => 'ff',
      'fallback_format' => DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    // The timezone override setting is not applicable to this formatter.
    $form['timezone_override']['#access'] = FALSE;

    $form['date_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Luxon date/time format'),
      '#description' => $this->t('See <a href="https://moment.github.io/luxon/#/formatting?id=table-of-tokens" target="_blank">the documentation for Luxon date format tokens</a>. Be sure to set a format appropriate for the field, i.e. omitting time for a field that only has a date.'),
      '#default_value' => $this->getSetting('date_format'),
      '#required' => TRUE,
    ];

    $form['fallback_format'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fallback (PHP) date/time format'),
      '#description' => $this->t('Set a fallback, server-side format for displaying the date. See <a href="https://www.php.net/manual/datetime.format.php#refsect1-datetime.format-parameters" target="_blank">the documentation for PHP date formats</a>.'),
      '#default_value' => $this->getSetting('fallback_format'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $summary[] = $this
      ->t('Format: @format', ['@format' => $this->getSetting('date_format')]);
    $summary[] = $this
      ->t('Fallback (server-side) format: @format', ['@format' => $this->getSetting('fallback_format')]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildDateWithIsoAttribute(DrupalDateTime $date) {
    $build = parent::buildDateWithIsoAttribute($date);

    // Create the ISO 8601 date in UTC.
    // Necessary due to Drupal core bug #3094501.
    // @see https://www.drupal.org/project/drupal/issues/3094501
    $iso_date = $date->format('c', ['timezone' => 'UTC']);
    $build['#attributes']['datetime'] = $iso_date;

    // Pass the luxon preset as a data attribute.
    $build['#attributes']['data-luxon-format'] = $this
      ->getSetting('date_format');

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date) {
    $format = $this->getSetting('fallback_format');
    $timezone = $date->getTimezone()->getName();
    return $this->dateFormatter->format($date->getTimestamp(), 'custom', $format, $timezone != '' ? $timezone : NULL);
  }

}

<?php

namespace Drupal\luxon_formatters\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeFormatterBase;
use Drupal\luxon_formatters\DateTimeTrait;

/**
 * Plugin implementation of the 'Luxon preset' formatter for 'datetime' fields.
 *
 * @FieldFormatter(
 *   id = "luxon_formatters_datetime_preset",
 *   label = @Translation("Luxon preset"),
 *   field_types = {
 *     "datetime"
 *   }
 * )
 */
class DateTimePresetFormatter extends DateTimeFormatterBase {

  use DateTimeTrait;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'date_format' => 'DATETIME_MED',
      'fallback_format' => 'medium',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    // The timezone override setting is not applicable to this formatter.
    $form['timezone_override']['#access'] = FALSE;

    $form['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Luxon date/time format'),
      '#description' => $this->t('See <a href="https://moment.github.io/luxon/#/formatting?id=presets" target="_blank">the documentation for Luxon date format presets</a>. Be sure to choose a format appropriate for the field, i.e. omitting time for a field that only has a date.'),
      '#options' => $this->presetOptions(),
      '#default_value' => $this->getSetting('date_format'),
      '#required' => TRUE,
    ];

    $time = new DrupalDateTime();
    $format_types = $this->dateFormatStorage->loadMultiple();
    $options = [];
    foreach ($format_types as $type => $type_info) {
      $format = $this->dateFormatter->format($time->getTimestamp(), $type);
      $options[$type] = $type_info->label() . ' (' . $format . ')';
    }
    $form['fallback_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Fallback (PHP) date/time format'),
      '#description' => $this->t("Choose a fallback, server-side format for displaying the date."),
      '#options' => $options,
      '#default_value' => $this->getSetting('fallback_format'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $options = $this->presetOptions();
    $summary[] = $this
      ->t('Format: @format', ['@format' => $options[$this->getSetting('date_format')]]);

    $date = new DrupalDateTime();
    $summary[] = $this
      ->t('Fallback (server-side) format: @format', ['@format' => $this->formatDate($date)]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildDateWithIsoAttribute(DrupalDateTime $date) {
    $build = parent::buildDateWithIsoAttribute($date);

    // Create the ISO 8601 date in UTC.
    // Necessary due to Drupal core bug #3094501.
    // @see https://www.drupal.org/project/drupal/issues/3094501
    $iso_date = $date->format('c', ['timezone' => 'UTC']);
    $build['#attributes']['datetime'] = $iso_date;

    // Pass the luxon preset as a data attribute.
    $build['#attributes']['data-luxon-preset'] = $this
      ->getSetting('date_format');

    return $build;
  }

  /**
   * Returns an array of known Luxon presets suitable for use in form elements.
   *
   * @see https://moment.github.io/luxon/#/formatting?id=presets
   *
   * @return array
   *   An array of known Luxon date format presets.
   */
  protected function presetOptions() {
    return [
      'DATE_SHORT' => $this->t('short date'),
      'DATE_MED' => $this->t('abbreviated date'),
      'DATE_SHORT' => $this->t('abbreviated date with abbreviated weekday'),
      'DATE_FULL' => $this->t('full date'),
      'DATE_HUGE' => $this->t('full date with weekday'),
      'TIME_SIMPLE' => $this->t('time'),
      'TIME_WITH_SECONDS' => $this->t('time with seconds'),
      'TIME_WITH_SHORT_OFFSET' => $this->t('time with seconds and abbreviated named offset'),
      'TIME_WITH_LONG_OFFSET' => $this->t('time with seconds and full named offset'),
      'TIME_24_SIMPLE' => $this->t('24-hour time'),
      'TIME_24_WITH_SECONDS' => $this->t('24-hour time with seconds'),
      'TIME_24_WITH_SHORT_OFFSET' => $this->t('24-hour time with seconds and abbreviated named offset'),
      'TIME_24_WITH_LONG_OFFSET' => $this->t('24-hour time with seconds and full named offset'),
      'DATETIME_SHORT' => $this->t('short date & time'),
      'DATETIME_MED' => $this->t('abbreviated date & time'),
      'DATETIME_FULL' => $this->t('full date and time with abbreviated named offset'),
      'DATETIME_HUGE' => $this->t('full date and time with weekday and full named offset'),
      'DATETIME_SHORT_WITH_SECONDS' => $this->t('short date & time with seconds'),
      'DATETIME_MED_WITH_SECONDS' => $this->t('abbreviated date & time with seconds'),
      'DATETIME_FULL_WITH_SECONDS' => $this->t('full date and time with abbreviated named offset with seconds'),
      'DATETIME_HUGE_WITH_SECONDS' => $this->t('full date and time with weekday and full named offset with seconds'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function formatDate($date) {
    $format_type = $this->getSetting('fallback_format');
    $timezone = $date->getTimezone()->getName();
    return $this->dateFormatter->format($date->getTimestamp(), $format_type, '', $timezone != '' ? $timezone : NULL);
  }

}

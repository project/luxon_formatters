<?php

namespace Drupal\luxon_formatters\Plugin\Field\FieldFormatter;

use Drupal\luxon_formatters\TimestampTrait;

/**
 * Plugin implementation of the 'Luxon custom' formatter for timestamp fields.
 *
 * @FieldFormatter(
 *   id = "luxon_formatters_timestamp_custom",
 *   label = @Translation("Luxon custom"),
 *   field_types = {
 *     "timestamp",
 *     "created",
 *     "changed",
 *   }
 * )
 */
class TimestampCustomFormatter extends DateTimeCustomFormatter {

  use TimestampTrait;

}

<?php

namespace Drupal\luxon_formatters\Plugin\Field\FieldFormatter;

use Drupal\luxon_formatters\TimestampTrait;

/**
 * Plugin implementation of the 'Luxon preset' formatter for timestamp fields.
 *
 * @FieldFormatter(
 *   id = "luxon_formatters_timestamp_preset",
 *   label = @Translation("Luxon preset"),
 *   field_types = {
 *     "timestamp",
 *     "created",
 *     "changed",
 *   }
 * )
 */
class TimestampPresetFormatter extends DateTimePresetFormatter {

  use TimestampTrait;

}

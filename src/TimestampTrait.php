<?php

namespace Drupal\luxon_formatters;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Provides shared methods for Luxon timestamp field formatters.
 */
trait TimestampTrait {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      if (is_numeric($item->value)) {
        /** @var \Drupal\Core\Datetime\DrupalDateTime $date */
        $date = DrupalDateTime::createFromTimestamp($item->value);
        $elements[$delta] = $this->buildDateWithIsoAttribute($date);

        if (!empty($item->_attributes)) {
          $elements[$delta]['#attributes'] += $item->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and should not be rendered in the field template.
          unset($item->_attributes);
        }
      }
    }

    return $elements;
  }

}
